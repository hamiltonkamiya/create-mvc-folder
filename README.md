** How to Create a MVC Folder as File System for XCode to your iOS WatchOS MacOS files**

> ## Show in Finder.
> 
> 1.   Blue Folder icon (on left top).
> 2.   Yellow Folder icon (below that on left top).
> 3.   RIGHT Click and select for "Show in Finder".
> 
> Here's some image example:

![Alt text](https://bitbucket.org/hamiltonkamiya/create-mvc-folder/src/master/Screenshots/Create%20MVC%20Folder1.png)

> ## Show in Finder.
> 
> 1.   Blue Folder icon (on left top).
> 2.   Yellow Folder icon (below that on left top).
> 3.   RIGHT Click and select for "Show in Finder".
> 
> Here's some image example:

![Alt text](https://bitbucket.org/hamiltonkamiya/create-mvc-folder/src/master/Screenshots/Create%20MVC%20Folder2.png)

> ## Show in Finder.
> 
> 1.   Blue Folder icon (on left top).
> 2.   Yellow Folder icon (below that on left top).
> 3.   RIGHT Click and select for "Show in Finder".
> 
> Here's some image example:

![Alt text](https://bitbucket.org/hamiltonkamiya/create-mvc-folder/src/master/Screenshots/Create%20MVC%20Folder3.png)

![Alt text](https://bitbucket.org/hamiltonkamiya/create-mvc-folder/src/master/Screenshots/Create%20MVC%20Folder4.png)

![Alt text](https://bitbucket.org/hamiltonkamiya/create-mvc-folder/src/master/Screenshots/Create%20MVC%20Folder5.png)

![Alt text](https://bitbucket.org/hamiltonkamiya/create-mvc-folder/src/master/Screenshots/Create%20MVC%20Folder6.png)



> ## Show in Finder.
> 
> 1.   text 1.
> 2.   text 2.
> 3.   text 3.
> 
> Here's some image example:
> 
>     return shell_exec("echo $input | $markdown_script");